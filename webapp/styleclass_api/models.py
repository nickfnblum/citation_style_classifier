from rest_framework import serializers


class CitationStyle:
    def __init__(self, ref_string, style):
        self.reference_string = ref_string
        self.citation_style = style


class Errors:
    def __init__(self, errors):
        self.errors = errors


class CitationStyleSerializer(serializers.Serializer):
    reference_string = serializers.CharField()
    citation_style = serializers.CharField()


class ErrorsSerializer(serializers.Serializer):
    errors = serializers.StringRelatedField(many=True)
