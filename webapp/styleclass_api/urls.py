from django.conf.urls import url, include

from . import views

urlpatterns = [
    # Using REST API
    url(r"^", include(views.styleclass_router.urls)),
    # Prometheus
    url("", include("django_prometheus.urls")),
]
